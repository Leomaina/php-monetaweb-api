<?php

namespace le0m\MonetaWeb\models;

use le0m\webapi\Model;


/**
 * Class ConfirmCancel
 *
 * Annulla conferma contabile **solo in giornata**.
 *
 * @property string $id
 * @property string $password
 * @property string $operationType
 * @property string $paymentId
 */
class ConfirmationCancel extends Model
{
	/**
	 * @inheritdoc
	 */
	function attributes()
	{
		return [
			'id' => [
				'required',
				'types' => ['numeric']
			],
			'password' => [
				'required',
				'types' => ['numeric']
			],
			'operationType' => [
				'required',
				'types' => ['string']
			],
			'paymentId' => [
				'required',
				'types' => ['string']
			]
		];
	}
}
