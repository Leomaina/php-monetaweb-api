<?php

namespace le0m\MonetaWeb\models;

use le0m\webapi\Model;


/**
 * Class Refund
 *
 * Storno contabile.
 *
 * @property string $id
 * @property string $password
 * @property string $operationType
 * @property double $amount
 * @property string $currencyCode
 * @property string $merchantOrderId
 * @property string $paymentId
 * @property string $customField
 * @property string $description
 */
class Refund extends Model
{
	/**
	 * @inheritdoc
	 */
	function attributes()
	{
		return [
			'id' => [
				'required',
				'types' => ['numeric']
			],
			'password' => [
				'required',
				'types' => ['numeric']
			],
			'operationType' => [
				'required',
				'types' => ['string']
			],
			'amount' => [
				'required',
				'types' => ['numeric']
			],
			'currencyCode' => [
				'types' => ['numeric']
			],
			'merchantOrderId' => [
				'required',
				'types' => ['string']
			],
			'paymentId' => [
				'required',
				'types' => ['string']
			],
			'customField' => [
				'types' => ['string']
			],
			'description' => [
				'types' => ['string']
			]
		];
	}
}
