<?php

namespace le0m\MonetaWeb\models;

use le0m\webapi\Model;


/**
 * Class InitializationResponse.
 *
 * To complete the payment you have to append **$paymentid** as a GET parameter to **$hostedpageurl** and redirect the user on the resulting URL.
 *
 * @property string $paymentid unique ID of the payment
 * @property string $securitytoken security token for this payment; you should check this when receiving server-to-server payment status notification
 * @property string $hostedpageurl approval url
 * @property string $errorcode error code
 * @property string $errormessage description of the error
 */
class InitializationResponse extends Model
{
	/**
	 * @inheritdoc
	 */
	public function validate()
	{
		parent::validate();

		if (isset($this->errorcode, $this->errormessage))
			$this->addError('paymentid', sprintf('[%s] $s', $this->errorcode, $this->errormessage));

		return $this->hasErrors();
	}

	/**
	 * @inheritdoc
	 */
	function attributes()
	{
		return [
			'paymentid' => [
				//'required',
				'types' => ['string']
			],
			'securitytoken' => [
				//'required',
				'types' => ['string']
			],
			'hostedpageurl' => [
				//'required',
				'types' => ['string']
			],
			// ERRORS
			'errorcode' => [
				'types' => ['string'],
			],
			'errormessage' => [
				'types' => ['string']
			]
		];
	}
}
