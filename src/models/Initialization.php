<?php

namespace le0m\MonetaWeb\models;

use le0m\webapi\Model;


/**
 * Class PaymentInit
 *
 * @property string $id ID del terminale
 * @property string $password password del terminale
 * @property string $operationType tipo dell'operazione
 * @property double $amount totale da pagare
 * @property string $currencyCode codice della valuta (opzionale, default a '978' per Euro)
 * @property string $language lingua della pagina di pagamento (DEU, FRA, ITA, POR, RUS, SPA, USA)
 * @property string $responseToMerchantUrl URL per la notifica dell'esito della transazione (server-to-server)
 * @property string $recoveryUrl URL di redirect in caso di errori nella comunicazione server-to-server (opzionale)
 * @property string $merchantOrderId ID di riferimento dell'ordine; alfanumerico e unico (max. 18 caratteri)
 * @property string $description descrizione del pagamento visualizzata nella pagina di pagamento (opzionale)
 * @property string $cardHolderName nome del titolare della carta (opzionale)
 * @property string $cardHolderEmail indirizzo email del titolare della carta, a cui notificare l'esito del pagamento (opzionale)
 * @property string $customField campo libero restituito in fase di notifica server-to-server (opzionale)
 */
class Initialization extends Model
{
	/**
	 * @inheritdoc
	 */
	public function attributes()
	{
		return [
			'id' => [
				'required',
				'types' => ['numeric']
			],
			'password' => [
				'required',
				'types' => ['numeric']
			],
			'operationType' => [
				'required',
				'types' => ['string']
			],
			'amount' => [
				'required',
				'types' => ['numeric']
			],
			'currencyCode' => [
				'types' => ['numeric']
			],
			'language' => [
				'required',
				'types' => ['string']
			],
			'responseToMerchantUrl' => [
				'required',
				'types' => ['string']
			],
			'recoveryUrl' => [
				'types' => ['string']
			],
			'merchantOrderId' => [
				'required',
				'types' => ['string']
			],
			'description' => [
				'types' => ['string']
			],
			'cardHolderName' => [
				'types' => ['string']
			],
			'cardHolderEmail' => [
				'types' => ['string']
			],
			'customField' => [
				'types' => ['string']
			]
		];
	}
}
