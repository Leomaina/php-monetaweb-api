<?php

namespace le0m\MonetaWeb\models;

use le0m\webapi\Model;


/**
 * Class Payment
 *
 * Si consiglia di aspettare **20 minuti** dalla creazione della transazione.
 *
 * @property string $id
 * @property string $password
 * @property string $operationType
 * @property string $paymentId
 *
 * @property string $result
 * @property string $paymentid
 * @property string $transactiontime
 * @property double $amount
 * @property string $currencycode
 * @property string $merchantorderid
 * @property string $authorizationcode
 * @property string $threedsecure
 * @property string $responsecode
 * @property string $customfield
 * @property string $description
 * @property string $rrn
 * @property string $cardbrand
 * @property string $cardtype
 * @property string $cardcountry
 * @property string $maskedpan
 * @property string $securitytoken
 * @property string $cardholderip
 * @property string $mybankid
 * @property string $selectedbank
 */
class Inquiry extends Model
{
	/**
	 * @inheritdoc
	 */
	function attributes()
	{
		return [
			// REQUEST
			'id' => [
				//'required',
				'types' => ['numeric']
			],
			'password' => [
				//'required',
				'types' => ['numeric']
			],
			'operationType' => [
				//'required',
				'types' => ['string']
			],
			'paymentId' => [
				//'required',
				'types' => ['string']
			],
			// RESPONSE
			'result' => [
				'types' => ['string']
			],
			'paymentid' => [
				'types' => ['string']
			],
			'transactiontime' => [
				'types' => ['string']
			],
			'amount' => [
				'types' => ['numeric']
			],
			'currencycode' => [
				'types' => ['numeric']
			],
			'merchantorderid' => [
				'types' => ['string']
			],
			'authorizationcode' => [
				'types' => ['string']
			],
			'threedsecure' => [
				'types' => ['string']
			],
			'responsecode' => [
				'types' => ['string']
			],
			'customfield' => [
				'types' => ['string']
			],
			'description' => [
				'types' => ['string']
			],
			'rrn' => [
				'types' => ['string']
			],
			'cardbrand' => [
				'types' => ['string']
			],
			'cardtype' => [
				'types' => ['string']
			],
			'cardcountry' => [
				'types' => ['string']
			],
			'maskedpan' => [
				'types' => ['string']
			],
			'securitytoken' => [
				'types' => ['string']
			],
			'cardholderip' => [
				'types' => ['string']
			],
			'mybankid' => [
				'types' => ['string']
			],
			'selectedbank' => [
				'types' => ['string']
			]
		];
	}
}
