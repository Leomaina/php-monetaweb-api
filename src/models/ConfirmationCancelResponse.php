<?php

namespace le0m\MonetaWeb\models;

use le0m\MonetaWeb\endpoints\Payment;
use le0m\webapi\Model;


/**
 * Class ConfirmCancelResponse
 *
 * @property string $paymentid
 * @property string $result
 * @property string $responsecode
 * @property string $authorizationcode
 * @property string $merchantorderid
 */
class ConfirmationCancelResponse extends Model
{
	/**
	 * @inheritdoc
	 */
	public function validate()
	{
		parent::validate();

		if ($this->result != Payment::RESULT_CONFIRMATION_CANCELLED)
			$this->addError('result', 'Confirmation not cancelled');

		if (isset($this->errorcode, $this->errormessage))
			$this->addError('paymentid', sprintf('[%s] %s', $this->errorcode, $this->errormessage));

		return $this->hasErrors();
	}

	/**
	 * @inheritdoc
	 */
	function attributes()
	{
		return [
			'paymentid' => [
				//'required',
				'types' => ['string']
			],
			'result' => [
				//'required',
				'types' => ['string']
			],
			'responsecode' => [
				//'required',
				'types' => ['string']
			],
			'authorizationcode' => [
				//'required',
				'types' => ['string']
			],
			'merchantorderid' => [
				//'required',
				'types' => ['string']
			],
		];
	}
}
