<?php

namespace le0m\MonetaWeb\models;

use le0m\MonetaWeb\endpoints\Payment;
use le0m\webapi\Model;


/**
 * Class AuthorizationCancelResponse
 *
 * @property string $paymentid
 * @property string $result
 * @property string $responsecode
 * @property string $authorizationcode
 * @property string $merchantorderid
 * @property string $description
 * @property string $customfield
 * @property string $errorcode
 * @property string $errormessage
 */
class AuthorizationCancelResponse extends Model
{
	/**
	 * @inheritdoc
	 */
	public function validate()
	{
		parent::validate();

		if ($this->result != Payment::RESULT_AUTHORIZATION_CANCELLED)
			$this->addError('result', 'Authorization not cancelled');

		if (isset($this->errorcode, $this->errormessage))
			$this->addError('paymentid', sprintf('[%s] %s', $this->errorcode, $this->errormessage));

		return $this->hasErrors();
	}

	/**
	 * @inheritdoc
	 */
	function attributes()
	{
		return [
			'paymentid' => [
				//'required',
				'types' => ['string']
			],
			'result' => [
				//'required',
				'types' => ['string']
			],
			'responsecode' => [
				//'required',
				'types' => ['string']
			],
			'authorizationcode' => [
				//'required',
				'types' => ['string']
			],
			'merchantorderid' => [
				//'required',
				'types' => ['string']
			],
			'description' => [
				'types' => ['string']
			],
			'customfield' => [
				'types' => ['string']
			],
			// ERRORS
			'errorcode' => [
				'types' => ['string'],
			],
			'errormessage' => [
				'types' => ['string']
			]
		];
	}
}
