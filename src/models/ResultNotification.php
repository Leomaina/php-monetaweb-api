<?php

namespace le0m\MonetaWeb\models;

use le0m\webapi\Model;
use le0m\MonetaWeb\endpoints\Payment;


/**
 * Class PaymentResult
 *
 * @property string $paymentid same as the one received on initialization
 * @property string $result transaction result
 * @property string $responsecode '000' if transaction authorized, or another value if transaction was denied
 * @property string $authorizationcode present only if transaction authorized
 * @property string $merchantorderid order ID reference, same as the one sent during initialization
 * @property string $threedsecure security level of the transaction
 * @property string $rrn unique transaction reference number, useful for explicit accounting through files
 * @property string $maskedpan masked credit card number
 * @property string $cardtype type of credit card used for payment by user
 * @property string $cardcountry credit card nationality
 * @property string $cardexpirydate credit card expiry date in 'mmYY' format
 * @property string $customfield custom field sent during initialization (optional)
 * @property string $securitytoken security token for this payment; compare this to the one received on initialization
 * @property string $errorcode error code
 * @property string $errormessage description of the error
 */
class ResultNotification extends Model
{
	/**
	 * @inheritdoc
	 */
	public function validate()
	{
		parent::validate();

		if ($this->result != Payment::RESULT_CAPTURED)
			$this->addError('result', 'Payment not captured');

		if (isset($this->errorcode, $this->errormessage))
			$this->addError('paymentid', sprintf('[%s] %s', $this->errorcode, $this->errormessage));

		return $this->hasErrors();
	}

	/**
	 * @inheritdoc
	 */
	function attributes()
	{
		return [
			'paymentid' => [
				'required',
				'types' => ['string']
			],
			'result' => [
				'required',
				'types' => ['string']
			],
			'responsecode' => [
				//'required',
				'types' => ['string']
			],
			'authorizationcode' => [
				//'required',
				'types' => ['string']
			],
			'merchantorderid' => [
				//'required',
				'types' => ['string']
			],
			'threedsecure' => [
				'required',
				'types' => ['string']
			],
			'rrn' => [
				//'required',
				'types' => ['string']
			],
			'maskedpan' => [
				//'required',
				'types' => ['string']
			],
			'cardtype' => [
				//'required',
				'types' => ['string']
			],
			'cardcountry' => [
				//'required',
				'types' => ['string']
			],
			'cardexpirydate' => [
				//'required',
				'types' => ['string']
			],
			'customfield' => [
				'types' => ['string']
			],
			'securitytoken' => [
				//'required',
				'types' => ['string']
			],
			// ERRORS
			'errorcode' => [
				'types' => ['string'],
			],
			'errormessage' => [
				'types' => ['string']
			]
		];
	}
}
