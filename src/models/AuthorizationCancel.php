<?php

namespace le0m\MonetaWeb\models;

use le0m\webapi\Model;


/**
 * Class AuthorizationCancel
 *
 * Annulla una transazione autorizzata (prima di conferma/contabilità)
 *
 * @property string $id
 * @property string $password
 * @property string $operationType
 * @property string $paymentId
 * @property string $customField
 * @property string $description
 */
class AuthorizationCancel extends Model
{
	/**
	 * @inheritdoc
	 */
	function attributes()
	{
		return [
			'id' => [
				'required',
				'types' => ['numeric']
			],
			'password' => [
				'required',
				'types' => ['numeric']
			],
			'operationType' => [
				'required',
				'types' => ['string']
			],
			'paymentId' => [
				'required',
				'types' => ['string']
			],
			'customField' => [
				'types' => ['string']
			],
			'description' => [
				'types' => ['string']
			]
		];
	}
}
