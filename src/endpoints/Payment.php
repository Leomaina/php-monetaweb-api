<?php

namespace le0m\MonetaWeb\endpoints;

use le0m\MonetaWeb\models\AuthorizationCancel;
use le0m\MonetaWeb\models\AuthorizationCancelResponse;
use le0m\MonetaWeb\models\Confirmation;
use le0m\MonetaWeb\models\ConfirmationCancel;
use le0m\MonetaWeb\models\ConfirmationCancelResponse;
use le0m\MonetaWeb\models\ConfirmationResponse;
use le0m\MonetaWeb\models\Inquiry;
use le0m\MonetaWeb\models\Refund;
use le0m\MonetaWeb\models\RefundResponse;
use le0m\webapi\BaseEndpoint;
use le0m\MonetaWeb\models\Initialization;
use le0m\MonetaWeb\models\InitializationResponse;


class Payment extends BaseEndpoint
{
	/** @const string PAYMENT_URL relative path to payment endpoint */
	const PAYMENT_URL = 'payment/2/xml';

	/**
	 * operation type constants
	 */
	const OPERATION_INIT = 'initialize';
	const OPERATION_CONFIRM = 'confirm';
	const OPERATION_REFUND = 'voidconfirmation';
	const OPERATION_CANCEL_AUTHORIZATION = 'voidauthorization';
	const OPERATION_CANCEL_CONFIRMATION = 'forcedvoidauthorization';
	const OPERATION_GET = 'inquiry';
	const OPERATION_GET_MYBANK = 'inquirymybank';
	const OPERATION_GET_ALIPAY = 'inquiryalipay';

	/**
	 * language code constants
	 */
	const LANGUAGE_GERMAN = 'DEU';
	const LANGUAGE_FRENCH = 'FRA';
	const LANGUAGE_ITALIAN = 'ITA';
	const LANGUAGE_PORTUGUESE = 'POR';
	const LANGUAGE_RUSSIAN = 'RUS';
	const LANGUAGE_SPANISH = 'SPA';
	const LANGUAGE_ENGLISH = 'USA';

	/**
	 * result code constants
	 */
	const RESULT_APPROVED = 'APPROVED'; // transazione autorizzata
	const RESULT_NOT_APPROVED = 'NOT APPROVED'; // transazione negata
	const RESULT_CAPTURED = 'CAPTURED'; // transazione confermata
	const RESULT_CANCELED = 'CANCELED'; // pagamento annullato dal titolare
	const RESULT_REFUNDED = 'VOIDED'; // transazione stornata (dopo conferma)
	const RESULT_AUTHORIZATION_CANCELLED = 'AUTH VOIDED'; // autorizzazione cancellata (pre pagamento)
	const RESULT_CONFIRMATION_CANCELLED = 'AUTH VOIDED'; // pagamento cancellato (solo in giornata)
	const RESULT_NOT_AUTHENTICATED = 'NOT AUTHENTICATED'; // autenticazione 3D fallita
	const RESULT_PARES_ERROR = 'PARES ERROR'; // errore in fase di autenticazione 3D (parES ?)
	const RESULT_MYBANK_AUTHORIZED = 'AUTHORISED'; // bonifico MyBank approvato (solo pagamento myBank)
	const RESULT_MYBANK_ERROR = 'ERROR'; // bonifico MyBank negato (solo pagamento MyBank)
	const RESULT_MYBANK_ABANDON = 'AUTHORISINGPARTYABORTED'; // bonifico MyBank abbandonato dall'utente (solo pagamenti MyBank)
	const RESULT_MYBANK_TIMEOUT = 'TIMEOUT'; // timeout bonifico MyBank (solo pagamento MyBank)
	const RESULT_MYBANK_PENDING = 'PENDING'; // bonifico MyBank non ancora completato (solo pagamento MyBank)


	/**
	 * Send a request to initialize a payment.
	 *
	 * @param Initialization $initModel
	 * @param bool $validate whether to validate the model before sending the request
	 *
	 * @return InitializationResponse|null null if model validation failed (use {@link Model::getErrors()} to retrieve a list of validation errors)
	 */
	public function initialize($initModel, $validate = true)
	{
		if ($validate && !$initModel->validate())
			return null;

		$response = $this->getClient()->request('post', self::PAYMENT_URL, [
			'query' => $initModel->toArray()
		]);
		$xml = simplexml_load_string($response->getBody(), "SimpleXMLElement", LIBXML_NOCDATA);
		$json = json_encode($xml);
		$array = json_decode($json, true);

		return new InitializationResponse($array);
	}

	/**
	 * Send a payment confirmation.
	 *
	 * @param Confirmation $confirmModel
	 * @param bool $validate whether to validate the model before sending the request
	 *
	 * @return ConfirmationResponse|null null if model validation failed (use {@link Model::getErrors()} to retrive a list of validation errrors)
	 */
	public function confirm($confirmModel, $validate = true)
	{
		if ($validate && !$confirmModel->validate())
			return null;

		$response = $this->getClient()->request('post', self::PAYMENT_URL, [
			'query' => $confirmModel->toArray()
		]);
		$xml = simplexml_load_string($response->getBody(), "SimpleXMLElement", LIBXML_NOCDATA);
		$json = json_encode($xml);
		$array = json_decode($json, true);

		return new ConfirmationResponse($array);
	}

	/**
	 * Send a refund request.
	 *
	 * @param Refund $refundModel
	 * @param bool $validate whether to validate the model before sending the request
	 *
	 * @return RefundResponse|null  null if model validation failed (use {@link Model::getErrors()} to retrive a list of validation errrors)
	 */
	public function refund($refundModel, $validate = true)
	{
		if ($validate && !$refundModel->validate())
			return null;

		$response = $this->getClient()->request('post', self::PAYMENT_URL, [
			'query' => $refundModel->toArray()
		]);
		$xml = simplexml_load_string($response->getBody(), "SimpleXMLElement", LIBXML_NOCDATA);
		$json = json_encode($xml);
		$array = json_decode($json, true);

		return new RefundResponse($array);
	}

	/**
	 * Cancels a payment authorization, before it has been confirmed.
	 *
	 * @param AuthorizationCancel $cancelModel
	 * @param bool $validate whether to validate the model before sending the request
	 *
	 * @return AuthorizationCancelResponse|null  null if model validation failed (use {@link Model::getErrors()} to retrive a list of validation errrors)
	 */
	public function cancelAuthorization($cancelModel, $validate = true)
	{
		if ($validate && !$cancelModel->validate())
			return null;

		$response = $this->getClient()->request('post', self::PAYMENT_URL, [
			'query' => $cancelModel->toArray()
		]);
		$xml = simplexml_load_string($response->getBody(), "SimpleXMLElement", LIBXML_NOCDATA);
		$json = json_encode($xml);
		$array = json_decode($json, true);

		return new AuthorizationCancelResponse($array);
	}

	/**
	 * Cancels a payment authorization, before it has been confirmed.
	 *
	 * @param ConfirmationCancel $cancelModel
	 * @param bool $validate whether to validate the model before sending the request
	 *
	 * @return ConfirmationCancelResponse|null  null if model validation failed (use {@link Model::getErrors()} to retrive a list of validation errrors)
	 */
	public function cancelConfirmation($cancelModel, $validate = true)
	{
		if ($validate && !$cancelModel->validate())
			return null;

		$response = $this->getClient()->request('post', self::PAYMENT_URL, [
			'query' => $cancelModel->toArray()
		]);
		$xml = simplexml_load_string($response->getBody(), "SimpleXMLElement", LIBXML_NOCDATA);
		$json = json_encode($xml);
		$array = json_decode($json, true);

		return new ConfirmationCancelResponse($array);
	}

	/**
	 * Get info about a transaction. Wait **20 minutes** before using.
	 *
	 * @param Inquiry $paymentModel
	 * @param bool $validate whether to validate the model before sending the request
	 *
	 * @return Inquiry|null  null if model validation failed (use {@link Model::getErrors()} to retrive a list of validation errrors)
	 */
	public function get($paymentModel, $validate = true)
	{
		if ($validate && !$paymentModel->validate())
			return null;

		$response = $this->getClient()->request('post', self::PAYMENT_URL, [
			'query' => $paymentModel->toArray()
		]);
		$xml = simplexml_load_string($response->getBody(), "SimpleXMLElement", LIBXML_NOCDATA);
		$json = json_encode($xml);
		$array = json_decode($json, true);

		return new Inquiry($array);
	}
}
