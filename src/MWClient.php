<?php

namespace le0m\MonetaWeb;

use le0m\webapi\BaseClient;
use le0m\MonetaWeb\endpoints\Payment;


class MWClient extends BaseClient
{
	/** @const string BASE_URL MonetaWeb base API URL */
	const BASE_URL = 'https://test.monetaonline.it/monetaweb/';


	/**
	 * MWClient constructor.
	 */
	public function __construct()
	{
		parent::__construct(self::BASE_URL);
	}

	/**
	 * No special headers, MonetaWeb uses NVP for requests.
	 *
	 * @return array
	 */
	public function headers()
	{
		return [];
	}

	/**
	 * Get Payment endpoint object.
	 *
	 * @return Payment
	 */
	public function payment()
	{
		return new Payment($this);
	}
}
